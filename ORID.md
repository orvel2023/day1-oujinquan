# O
  - IPM、kick off、Desk Check :  Today, I have a clearer understanding of these concepts, which are mainly used in the agile process to ensure that the functionality is correct, to ensure that the requirements are correct, and to ensure that the bug code is not committed to the production environment during the development process.
  - Agile activity： A clear understanding of the roles in Agile development and how they work together. How to develop a project from 0 to 1, analyze requirements, identify user pain points, create a user journey, select and develop an MVP product, and then determine the development story.
  - Teamwork: In today's team cooperation, I know how Tech leader, BA,DEV,UX and QA should cooperate. A good BA plays a great role in the smooth progress of team development, and tech leader takes the lead role in technology for the team, so that the project can be implemented more smoothly. In general, Good agile development requires the joint efforts of every member of the team in order to complete the project better and faster.
# R
  - Helpful !
# I
  - Today's learning has given me a taste of the agile process, which will make me more familiar with the team agile development in the future.
# D
  - I will deepen my understanding of Agile activities during next week's simulation project.
 
